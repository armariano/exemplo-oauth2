package br.com.mastertech.oauth.controllers;

import br.com.mastertech.oauth.models.Usuario;
import br.com.mastertech.oauth.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/contato")
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

    @GetMapping
    public ResponseEntity<?> getUsuarios(@AuthenticationPrincipal Usuario usuario) {
        try {
            List<Usuario> usuarios = usuarioService.buscarTodosContatos(usuario.getNome());
            return ResponseEntity.status(201).body(usuarios);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<?> salvarUsuario(@AuthenticationPrincipal Usuario usuario) {
        try {
            Usuario usuarioRetorno = usuarioService.salvarUsuario(usuario);
            return ResponseEntity.status(201).body(usuarioRetorno);
        } catch (RuntimeException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
