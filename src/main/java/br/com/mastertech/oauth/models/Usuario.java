package br.com.mastertech.oauth.models;

import javax.persistence.*;

@Entity
@Table
public class Usuario {

    public Usuario() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column
    public String nome;

    @Column
    public String contato;

    public Integer getId() {
        return id;
    }

    public Usuario setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public Usuario setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getContato() {
        return contato;
    }

    public Usuario setContato(String contato) {
        this.contato = contato;
        return this;
    }
}
