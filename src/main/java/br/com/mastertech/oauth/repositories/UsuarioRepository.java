package br.com.mastertech.oauth.repositories;

import br.com.mastertech.oauth.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    List<Usuario> findAllByNome(String nome);
}
