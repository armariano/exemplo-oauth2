package br.com.mastertech.oauth.services;

import br.com.mastertech.oauth.models.Usuario;
import br.com.mastertech.oauth.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class UsuarioService {

    @Autowired
    UsuarioRepository usuarioRepository;

    public Usuario salvarUsuario(Usuario usuario) {
        Usuario usuarioSave = new Usuario();
        usuarioSave.setNome(usuario.getNome());
        usuarioSave.setContato(gerarContatoRandomico());

        return usuarioRepository.save(usuarioSave);
    }

    public List<Usuario> buscarTodosContatos(String nome) {
        return usuarioRepository.findAllByNome(nome);
    }

    public String gerarContatoRandomico() {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random rd = new Random();
        String generatedString = rd.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return generatedString;
    }
}
